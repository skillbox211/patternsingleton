#include <iostream>

/////////////////////////////////////////////////////////////////////////////////////////
//// VikBo - pattern Singleton
/////////////////////////////////////////////////////////////////////////////////////////


class UnsafeSingleton
{
protected:
    UnsafeSingleton() = default;
    static UnsafeSingleton* MyInstance;
    virtual ~UnsafeSingleton() = default;
public:

    static UnsafeSingleton* Get(){
        if(!MyInstance)
        {
            MyInstance = new UnsafeSingleton();
            std::printf("Creating Singleton...\n");
        }
        std::printf("Singleton yet creating.\n");
        return MyInstance;
    }

    static bool Delete()
    {
        if(MyInstance)
        {
            delete MyInstance;
            MyInstance = nullptr;
            std::printf("Singleton delete\n");
            return true;
        }
        return false;
    }

    UnsafeSingleton(UnsafeSingleton &other) = delete;
    void operator=(const UnsafeSingleton &) = delete;

};

// Singleton creating this
UnsafeSingleton* UnsafeSingleton ::MyInstance = nullptr;

int main() {

    UnsafeSingleton* b  = UnsafeSingleton::Get();

    UnsafeSingleton::Delete();
//    NOT WORKED!!!
//    UnsafeSingleton* ptr_b = new UnsafeSingleton();
//    UnsafeSingleton b = UnsafeSingleton();

    return 0;
}
